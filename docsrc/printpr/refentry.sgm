<reference><title>TOC/LOT Apparatus</title>

&generate-set-toc;
&generate-book-toc;
&generate-book-lot-list;
&generate-part-toc;
&generate-part-toc-on-titlepage;
&generate-reference-toc;
&generate-reference-toc-on-titlepage;
&generate-article-toc;
&generate-article-toc-on-titlepage;

</reference>
<reference><title>Titlepages</title>

&generate-set-titlepage;
&generate-book-titlepage;
&generate-part-titlepage;
&generate-partintro-on-titlepage;
&generate-reference-titlepage;
&generate-article-titlepage;
&generate-article-ttlpg-on-sep-page;
&titlepage-in-info-order;
&othername-in-middle;

</reference>
<reference><title>RefEntries and FuncSynopses</title>

&refentry-new-page;
&refentry-keep;
&refentry-generate-name;
&refentry-xref-italic;
&refentry-xref-manvolnum;
&funcsynopsis-style;
&kr-funcsynopsis-indent;
&funcsynopsis-decoration;

</reference>
<reference><title>Fonts</title>

&refentry-name-font-family;
&title-font-family;
&body-font-family;
&mono-font-family;
&admon-font-family;
&guilabel-font-family;
&visual-acuity;
&hsize-bump-factor;
&smaller-size-factor;
&ss-size-factor;
&ss-shift-factor;
&verbatim-size-factor;
&bf-size;
&footnote-size-factor;
&formal-object-title-font-weight;
&table-title-font-weight;

</reference>
<reference><title>Backends</title>

&tex-backend;
&mif-backend;
&rtf-backend;
&default-backend;
&print-backend;

</reference>
<reference><title>Verbatim Environments</title>

&verbatim-default-width;
&number-synopsis-lines;
&number-funcsynopsisinfo-lines;
&number-literallayout-lines;
&number-address-lines;
&number-programlisting-lines;
&number-screen-lines;
&linenumber-mod;
&linenumber-length;
&linenumber-padchar;
&linenumber-space;
&indent-synopsis-lines;
&indent-funcsynopsisinfo-lines;
&indent-literallayout-lines;
&indent-address-lines;
&indent-programlisting-lines;
&indent-screen-lines;
&callout-fancy-bug;
&callout-default-col;

</reference>
<reference><title>Labelling</title>

&section-autolabel;
&chapter-autolabel;
&label-preface-sections;
&qanda-inherit-numeration;

</reference>
<reference><title>Running Heads</title>

&chap-app-running-heads;
&chap-app-running-head-autolabel;

</reference>
<reference><title>Paper/Page Characteristics</title>

&paper-type;
&two-side;
&writing-mode;
&page-n-columns;
&titlepage-n-columns;
&page-column-sep;
&page-balance-columns;
&left-margin;
&right-margin;
&page-width;
&page-height;
&text-width;
&epigraph-start-indent;
&epigraph-end-indent;
&body-width;
&top-margin;
&bottom-margin;
&header-margin;
&footer-margin;
&page-number-restart;
&article-page-number-restart;
&generate-heading-level;

</reference>
<reference><title>Admonitions</title>

&admon-graphics;
&admon-graphics-path;
&admon-graphic-default-extension;
&admon-graphic-extension;
&admon-graphic;
&admon-graphic-width;

</reference>
<reference><title>Quadding</title>

&default-quadding;
&division-title-quadding;
&division-subtitle-quadding;
&component-title-quadding;
&component-subtitle-quadding;
&article-title-quadding;
&article-subtitle-quadding;
&section-title-quadding;
&section-subtitle-quadding;

</reference>
<reference><title>Bibliographies</title>

&biblio-citation-check;
&filter-used;
&biblio-number;
&biblio-xref-title;

</reference>
<reference><title>OLinks</title>

&olink-outline-ext;

</reference>
<reference><title>Footnotes</title>

&footnote-ulinks;
&bop-footnotes;

</reference>
<reference><title>Graphics</title>

&graphic-default-extension;
&graphic-extensions;
&image-library;
&image-library-filename;

</reference>
<reference><title>Tables</title>

&table-element-list;
&simplelist-column-width;

</reference>
<reference><title>VariableLists</title>

&default-variablelist-termlength;
&may-format-variablelist-as-table;
&always-format-variablelist-as-table;

</reference>
<reference><title>Vertical Spacing</title>

&line-spacing-factor;
&head-before-factor;
&head-after-factor;
&body-start-indent;
&X25blockquote-start-indentX25;
&X25blockquote-end-indentX25;
&para-sep;
&block-sep;

</reference>
<reference><title>Indents</title>

&para-indent;
&para-indent-firstpara;
&block-start-indent;

</reference>
<reference><title>Object Rules</title>

&example-rules;
&figure-rules;
&table-rules;
&equation-rules;
&informalexample-rules;
&informalfigure-rules;
&informaltable-rules;
&informalequation-rules;
&object-rule-thickness;

</reference>
<reference><title>Miscellaneous</title>

&object-titles-after;
&formal-object-float;
&default-title-end-punct;
&content-title-end-punct;
&honorific-punctuation;
&default-simplesect-level;
&show-ulinks;
&show-comments;
&firstterm-bold;
&min-leading;
&hyphenation;

</reference>
